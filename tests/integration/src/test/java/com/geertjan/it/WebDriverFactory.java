package com.geertjan.it;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.HttpHostConnectException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;

public class WebDriverFactory {

//    private static final Logger LOG = LoggerFactory.getLogger(WebDriverFactory.class);
    private static WebDriver webDriver;

    private static final String PHANTOM_JS_PATH = "/Users/GJDB/phantomjs-2.0.0-macosx/bin/phantomjs";

    public static WebDriver createInstance() {
        if (isConnectedToBrowser()) {
//            LOG.info("Removing cookies");
            webDriver.manage().deleteAllCookies();
        }

        if (webDriver != null) {
            webDriver.close();
        }

//        LOG.info("Starting browser...");
        initializePhantomJSDriver();
//        LOG.info("Browser is started");

        if (!isConnectedToBrowser()) {
//            LOG.info("Not connected to browser, starting new browser...");
            initializePhantomJSDriver();
//            LOG.info("Browser is started");
        }

        return webDriver;
    }

    public static WebDriver getWebDriver() {
        return webDriver;
    }

    private static boolean isConnectedToBrowser() {
        if (webDriver == null) {
            return false;
        }
        try {
            webDriver.getCurrentUrl();
        } catch (final RuntimeException e) {
            if (e instanceof WebDriverException) {
                final WebDriverException webDriverException = (org.openqa.selenium.WebDriverException) e;
                if (webDriverException.getCause() != null && webDriverException.getCause() instanceof HttpHostConnectException){
                    return false;
                }
            }
        }
        return true;
    }

    public static void initializePhantomJSDriver() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.phantomjs();
        desiredCapabilities.setCapability("phantomjs.binary.path", PHANTOM_JS_PATH);
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        webDriver = new PhantomJSDriver(desiredCapabilities);
    }

    public static void takeScreenshot() {
        File screenshot = ((PhantomJSDriver) webDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("c:\\tmp\\screenshot.png"));
        } catch (IOException e) {
//            LOG.error();
        }
    }

}