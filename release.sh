#!/bin/sh

# Preconditions -   The parent does not have any snapshot dependencies.
#               -   There are no pending local modifications
#               -   The tagbase is @{artifactId}/@{artifactId}-@{version}

# After this script you need to manually verify the changes, test, commit and push these. Additionally when you
# want to use a snapshot version of the parent in a module, you'd best temporarily activate the relativePath in that
# module.

# loop all modules and check for modifications, when there are modifications release the module and check for
# usages of that module


# do not echo
set +v

set CURDIR=$CD

# assume you are already in the directory of the maven module you want to check
function moduleHasModificationsSinceLastRelease() {
    mkdir target
    ARTIFACTTAG="$1/$1"

    # sort on date to prevent string based sorting of numbers. This prevents the following order 1 10 11 2 3 4 5 6 7 8 9
    git tag | xargs -I@ git log --format=format:"%ai @%n" -1 @ | sort | awk '{print $4}' > target/tags.txt

    # cat -n prints the file to STDOUT prepending line numbers.
    # grep greps out all lines containing "b" (you can use egrep for more advanced patterns or fgrep for faster grep of fixed strings)
    # tail -1 prints last line of those lines containing "b"
    # cut -f 2 prints second column, which is line # from cat -n
    MODULETAG=$(cat -n target/tags.txt | grep $ARTIFACTTAG | tail -1 |  cut -f 2)

    echo "The tag of the last release of module $ARTIFACTTAG is $MODULETAG"

    mvn scm:changelog -DstartScmVersion=$MODULETAG -DscmVersionType=tag > target/changelog.txt

    # TODO verify making the release is not the only change!
    if grep -R "modified]:$1" target/changelog.txt || grep -R "added]:$1" target/changelog.txt || grep -R "deleted]:$1" target/changelog.txt
    then
        echo "$1 has changed since its last release"
        return 0 # means true / success
    else
        echo "$1 has not changed since its last release"
        return 1 # means false / failure
    fi

}

function releaseModule() {
    mvn release:prepare -B && mvn release:perform
}

function checkForLocalModifications() {
    mkdir target
    mvn org.apache.maven.plugins:maven-scm-plugin:1.9.2:check-local-modification > target/modifications.txt
    if grep -R "The build will stop as there is local modifications" target/modifications.txt
    then
        echo "There are local modifications"
        exit
    else
        echo "There are NO local modifications"
    fi
}

mvn clean
#checkForLocalModifications

CURRENTARTIFACT="parent"
cd $CURRENTARTIFACT

if moduleHasModificationsSinceLastRelease $CURRENTARTIFACT
then
   releaseModule

   # use the new parent in the whole project
   cd ..
   mvn versions:update-parent
else
    echo "$CURRENTARTIFACT has not changed"
fi

mvn versions:use-latest-releases -Dincludes="com.geertjan.it"
mvn versions:use-releases


